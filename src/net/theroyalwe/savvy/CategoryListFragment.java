package net.theroyalwe.savvy;

import java.io.IOException;
import java.util.ArrayList;

import org.apache.http.client.ClientProtocolException;
import org.json.JSONException;

import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.View;
import android.widget.ListView;

import com.actionbarsherlock.app.SherlockListFragment;

public class CategoryListFragment extends SherlockListFragment {

	private static String TAG = "CATEGORY";

	private boolean categoriesPopulated = false;
	private Integer userid;

	private ArrayList<SavvyItem> itemsForUser;

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {

		super.onActivityCreated(savedInstanceState);

		// /find out how to pass in arguments and pass in a users list to
		// repurpose this view!!!
		Bundle args = getArguments();
		// Log.v(TAG, (String) args.get("user"));
		if (getArguments() != null && getArguments().containsKey("userid")) {
			// SharedPreferences prefs =
			// PreferenceManager.getDefaultSharedPreferences(getActivity());
			// userid = prefs.getInt("registered", 0);
			userid = Integer.valueOf(getArguments().getString("userid"));

		}

		// if(!categoriesPopulated){

		Log.v(TAG, MainActivity.categories.toString());
		Log.v(TAG,
				"Showing " + Integer.toString(MainActivity.categories.size()));
		// if(MainActivity.categories.size() == 0){
		new GetCategoriesTask().execute();
		// categoriesPopulated = true;
		// }
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
	}

	@Override
	public void onPause() {
		super.onPause();
	}

	public void onResume() {
		super.onResume();
	}

	@Override
	public void onListItemClick(ListView l, View v, int position, long id) {
		// Do something with the data
		SavvyCategory category = (SavvyCategory) l.getItemAtPosition(position);
		CategoryInfoFragment categoryInfoFragment = new CategoryInfoFragment();
		Bundle bundle = new Bundle();

		if (userid != null) {
			ArrayList<SavvyItem> itemsOfType = new ArrayList<SavvyItem>();
			for (SavvyItem item : itemsForUser) {
				if (item.getCatId() == category.getId()) {
					itemsOfType.add(item);
				}
			}

			bundle.putParcelableArrayList("useritems", itemsOfType);
		}

		bundle.putParcelable(getString(R.string.CATEGORY_KEY), category);
		categoryInfoFragment.setArguments(bundle);
		FragmentTransaction ft = getFragmentManager().beginTransaction();
		ft.replace(getId(), categoryInfoFragment);
		ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
		ft.addToBackStack(null);
		ft.commit();

	}

	private void showFullList() {
		if (MainActivity.categories != null) {
			SavvyCategory[] categoryArray = MainActivity.categories
					.toArray(new SavvyCategory[MainActivity.categories.size()]);
			CategoryAdapter adapter = new CategoryAdapter(getActivity(),
					R.layout.listview_item_row, categoryArray);
			setListAdapter(adapter);
			categoriesPopulated = true;
		}
	}

	private void showCategoriesForUserItems() {
		if (MainActivity.categories != null) {

			ArrayList<Integer> catids = new ArrayList<Integer>();
			for (SavvyItem item : itemsForUser) {
				if (!catids.contains(item.catId)) {
					catids.add(item.catId);
				}
			}
			// now we have a list of all the categories of items that the user
			// has purchased
			ArrayList<SavvyCategory> purchasedCategories = new ArrayList<SavvyCategory>();
			for (Integer i : catids) {
				for (SavvyCategory category : MainActivity.categories) {
					if (category.getId() == i) {
						purchasedCategories.add(category);
					}
				}
			}

			SavvyCategory[] categoryArray = purchasedCategories
					.toArray(new SavvyCategory[purchasedCategories.size()]);
			CategoryAdapter adapter = new CategoryAdapter(getActivity(),
					R.layout.listview_item_row, categoryArray);
			setListAdapter(adapter);
		}

	}

	private class GetCategoriesTask extends
			SavvyAsyncTask<String, Integer, Boolean> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute(getActivity());
		}

		@Override
		protected void onPostExecute(Boolean result) {
			// update The list, update UI
			Log.v(TAG, "the userid is " + userid);
			if (userid != null) {
				new GetItemsForUserTask().execute();
			} else {
				showFullList();
			}
		}

		@Override
		protected Boolean doInBackground(String... args) {
			// make the http call

			try {
				if (MainActivity.categories.size() == 0) {
					MainActivity.categories = new SavvyAPI().getCategories();
				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ClientProtocolException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			return null;
		}

	}

	private class GetItemsForUserTask extends
			SavvyAsyncTask<String, Integer, Boolean> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute(getActivity());
		}

		@Override
		protected void onPostExecute(Boolean result) {
			showCategoriesForUserItems();
		}

		@Override
		protected Boolean doInBackground(String... params) {
			try {
				// TODO Auto-generated method stub

				itemsForUser = new SavvyAPI().getItemsByUser(userid.toString());

			} catch (ClientProtocolException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return null;
		}

	}

}
