package net.theroyalwe.savvy;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

public class SavvyAPI {

	
	private static String TAG = "SAVVY.API";
	
	private static String BASE_URL = "http://savvy.theroyalwe.net";
	private static String SECURE_BASE_URL = BASE_URL;//"http://theroyalwe.net/~savvy";

	final String SENDER_ID = "800377911519";
	
	HttpClient client = new DefaultHttpClient();

	// HttpResponse response;

	private String processResponse(HttpResponse response) {

		StringBuilder stringBuilder = new StringBuilder();
		try {

			HttpEntity entity = response.getEntity();
			InputStream stream = entity.getContent();
			int b;
			while ((b = stream.read()) != -1) {
				stringBuilder.append((char) b);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		//Log.v(TAG, "RESPONSE:"+stringBuilder.toString());
		return stringBuilder.toString();

	}
	// Get request to the Server using URL parameter
	private String makeGetRequest(String url) throws ClientProtocolException,
			IOException {

		HttpGet httpGet = new HttpGet(url);

		return processResponse(client.execute(httpGet));

	}
	// Post request to the Server using URL and parameters using name value pairs
	private String makePostRequest(String url,
			List<NameValuePair> nameValuePairs) throws ClientProtocolException,
			IOException {

		HttpPost httpPost = new HttpPost(url);
		httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
		String responseString = processResponse(client.execute(httpPost));
		Log.v(TAG, "Response:"+responseString);
		return responseString;

	}
	private int parseUserId(String registerUserResponseString) throws JSONException{
		int registerUserId = 0;
	
			JSONObject jsonResponse = new JSONObject(registerUserResponseString);
			JSONObject response = jsonResponse.getJSONObject("response");
			//int worked = response.getInt("worked");
			
			/*(if(worked != 0){
				registerUserId = response.getInt("userid");
			}*/
			if(response.has("userid")){
				registerUserId = response.getInt("userid");
			}
			
	Log.v(TAG, "got back userid:"+registerUserId);
		return registerUserId;
	}
	
	
	// From each JSONObject (SavvyItem stored in Database, this method prepare and return SavvyItem object
	private SavvyItem generateSavvyItem(JSONObject savvyItemJsonObject) throws JSONException{
		SavvyItem item = new SavvyItem();
		item.setCatId(savvyItemJsonObject.getInt("catid"));
		item.setId(savvyItemJsonObject.getInt("id"));
		item.setLat(new Float(savvyItemJsonObject.getString("lat")));
		item.setLon(new Float(savvyItemJsonObject.getString("lon")));
		item.setName(savvyItemJsonObject.getString("name"));
		item.setPrice(savvyItemJsonObject.getDouble("price"));
		item.setQuantity(savvyItemJsonObject.getInt("quantity"));
		item.setUserId(savvyItemJsonObject.getInt("userid"));
		item.setTs(savvyItemJsonObject.getString("ts"));
		item.setRating(new Float(savvyItemJsonObject.getString("rating")));
		
		return item;
	}
	
	// return arrayList of SavvyItem objects
	private ArrayList<SavvyItem> generateSavvyItemArrayList(
			String itemResponseString) throws JSONException {
		ArrayList<SavvyItem> items = new ArrayList<SavvyItem>();
		JSONObject itemsJSON = new JSONObject(itemResponseString);

		JSONArray itemsJSONArray = (JSONArray) itemsJSON.get("items");
		JSONObject jObj = null;
		for (int i = 0; i < itemsJSONArray.length(); i++) {
			jObj = (JSONObject) itemsJSONArray.get(i);
			SavvyItem item = generateSavvyItem(jObj);
			items.add(item);
		}

		return items;
	}

	// Method to retrieve cateogory list from server
	// Input params - none
	// output - ArrayList of SavvyCategory objects
	public ArrayList<SavvyCategory> getCategories() throws JSONException,
			ClientProtocolException, IOException {
		String categoryResponseString = this.makeGetRequest(SECURE_BASE_URL
				+ "/category");
		ArrayList<SavvyCategory> categories = new ArrayList<SavvyCategory>();
		JSONObject categoryJSON = new JSONObject(categoryResponseString);

		// parse JSON
		JSONArray categoryJSONArray = (JSONArray) categoryJSON
				.get("categories");
		JSONObject jObj  = null;
		for (int i = 0; i < categoryJSONArray.length(); i++) {
			SavvyCategory category = new SavvyCategory();
			jObj = (JSONObject) categoryJSONArray.get(i);
			category.setName((jObj).getString("name"));
			category.setId((jObj).getInt("id"));
			category.setPicUrl((jObj).getString("picurl"));
			categories.add(category);
		}
		return categories;

	}

	public ArrayList<SavvyItem> getItemsForCategoryByName(String categoryName)
			throws JSONException, ClientProtocolException, IOException {
		String itemResponseString = this.makeGetRequest(SECURE_BASE_URL + "/items/"
				+ categoryName + "/");
		return this.generateSavvyItemArrayList(itemResponseString);

	}

	public ArrayList<SavvyItem> getItemsForCategoryByCategoryId(int categoryID)
			throws JSONException, ClientProtocolException, IOException {

		String itemResponseString = this.makeGetRequest(SECURE_BASE_URL + "/items/"
				+ Integer.toString(categoryID) + "/");
		return this.generateSavvyItemArrayList(itemResponseString);

	}

	public ArrayList<SavvyItem> findItemsInRadiusOfLatLon(float lat, float lon,
			double radius) throws JSONException, ClientProtocolException,
			IOException {

		String itemResponseString = this.makeGetRequest(SECURE_BASE_URL
				+ "/search/within/" + Float.toString(lat) + ","
				+ Float.toString(lon) + "," + Double.toString(radius) + "/");

		return this.generateSavvyItemArrayList(itemResponseString);

	}

	public ArrayList<SavvyItem> findItemsInRadiusOfLatLon(String name,
			float lat, float lon, double radius) throws JSONException,
			ClientProtocolException, IOException {

		String itemResponseString = this.makeGetRequest(SECURE_BASE_URL + "/search/"
				+ name + "/within/" + Float.toString(lat) + ","
				+ Float.toString(lon) + "," + Double.toString(radius) + "/");

		return this.generateSavvyItemArrayList(itemResponseString);

	}

	public ArrayList<SavvyItem> findItemsInRadiusOfLatLon(String name,
			float lat, float lon, double radius, double ageInHours)
			throws JSONException, ClientProtocolException, IOException {

		String itemResponseString = this.makeGetRequest(SECURE_BASE_URL + "/search/"
				+ name + "/within/" + Float.toString(lat) + ","
				+ Float.toString(lon) + "," + Double.toString(radius) + "/"
				+ Double.toString(ageInHours) + "/");
		return this.generateSavvyItemArrayList(itemResponseString);

	}

	public int registerUser(String name, String email, String regId)
			throws ClientProtocolException, IOException, JSONException {

		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
		nameValuePairs.add(new BasicNameValuePair("name", name));
		nameValuePairs.add(new BasicNameValuePair("email", email));
		nameValuePairs.add(new BasicNameValuePair("regId", regId));

		//secure_base_url might not be working via server configuration.  I will fix it.
		/*if (this.makePostRequest(BASE_URL+"/gcm/register.php", nameValuePairs).contains("success")){
			return true;
		}else{
			return false;
		}*/
		
		return parseUserId(this.makePostRequest(BASE_URL+"/gcm/register.php", nameValuePairs));

	}
	
	public ArrayList<SavvyItem> postSavvyItem(String catid, String lat, String lon, String userid, String price, String quantity, String rating )throws ClientProtocolException, IOException, JSONException{
		Log.v(TAG, "posint item:"+catid+","+lat+","+lon+","+userid+","+price+","+quantity+","+rating);
		
		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
		nameValuePairs.add(new BasicNameValuePair("catid", catid));
		nameValuePairs.add(new BasicNameValuePair("lat", lat));
		nameValuePairs.add(new BasicNameValuePair("lon", lon));
		nameValuePairs.add(new BasicNameValuePair("price", price));
		nameValuePairs.add(new BasicNameValuePair("userid", userid));
		nameValuePairs.add(new BasicNameValuePair("quantity", quantity));
		nameValuePairs.add(new BasicNameValuePair("rating", rating));
		
		return generateSavvyItemArrayList(this.makePostRequest(SECURE_BASE_URL+"/items/new.php", nameValuePairs));
		
	}
	public ArrayList<SavvyItem> getItemsByUser(String userid) throws ClientProtocolException, IOException, JSONException{
		String itemResponseString = this.makeGetRequest(SECURE_BASE_URL +"/items/user/"+userid+"/");
		
		return this.generateSavvyItemArrayList(itemResponseString);
		
	}

}
