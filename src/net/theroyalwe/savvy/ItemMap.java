package net.theroyalwe.savvy;

import android.os.Bundle;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class ItemMap extends SupportMapFragment {
	SavvyItem mitem;

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		mitem = (SavvyItem) getArguments().getParcelable(
				getString(R.string.ITEM_KEY));

		this.getMap().addMarker(
				new MarkerOptions()
						.title(mitem.getName())
						.position(new LatLng(mitem.getLat(), mitem.getLon()))
						.icon(BitmapDescriptorFactory
								.fromResource(R.drawable.ic_launcher))
						.visible(true));

		this.getMap().moveCamera(
				CameraUpdateFactory.newLatLng(new LatLng(mitem.getLat(), mitem
						.getLon())));
		this.getMap().moveCamera(CameraUpdateFactory.zoomTo(16));

	}

}
