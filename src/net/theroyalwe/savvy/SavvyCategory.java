package net.theroyalwe.savvy;

import android.graphics.Bitmap;
import android.os.Parcel;
import android.os.Parcelable;

public class SavvyCategory implements Parcelable {
	int id;
	String name;
	String picUrl;
	Bitmap image = null;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPicUrl() {
		return picUrl;
	}
	public void setPicUrl(String picUrl) {
		this.picUrl = picUrl;
	}
	public Bitmap getImage(){
		return image;
	}
	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}
	@Override
	public void writeToParcel(Parcel dest, int flags) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public int hashCode(){
		return (int) (this.name.hashCode() % this.id);
		
	}
	@Override
	public boolean equals(Object obj){
		if (obj == null)
            return false;
        if (obj == this)
            return true;
        if (obj.getClass() != getClass())
            return false;
        SavvyCategory i = (SavvyCategory)obj;
        return this.id == i.id;
	}
}
