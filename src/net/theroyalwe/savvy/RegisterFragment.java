package net.theroyalwe.savvy;

import java.io.IOException;

import org.apache.http.client.ClientProtocolException;
import org.json.JSONException;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.actionbarsherlock.app.SherlockFragment;
import com.google.android.gcm.GCMRegistrar;

public class RegisterFragment extends SherlockFragment {
	private static String TAG = "Register";

	EditText textName;
	EditText textEmail;
	String name;
	String email;
	String regId;

	Button buttonRegister;
	SharedPreferences prefs;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

		prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());

		Log.v(TAG,
				"loading " + prefs.getString("name", "name") + ","
						+ prefs.getString("email", "email") + ","
						+ prefs.getString("regId", "regId"));

		// Make sure the device has the proper dependencies.
		GCMRegistrar.checkDevice(this.getActivity());

		// Make sure the manifest was properly set - comment out this line
		// while developing the app, then uncomment it when it's ready.
		GCMRegistrar.checkManifest(this.getActivity());

		// registerReceiver(MainActivity.mHandleMessageReceiver, new
		// IntentFilter(DISPLAY_MESSAGE_ACTION));

		View myFragmentView = inflater.inflate(R.layout.register_user,
				container, false);

		textName = (EditText) myFragmentView.findViewById(R.id.txtName);
		textEmail = (EditText) myFragmentView.findViewById(R.id.txtEmail);
		buttonRegister = (Button) myFragmentView.findViewById(R.id.btnRegister);

		if (prefs.getString("name", "name") != null) {

			textName.setText(prefs.getString("name", name));
		}
		if (prefs.getString("email", "email") != null) {
			textEmail.setText(prefs.getString("email", email));
		}

		// for testing
		// textName.setText("Scott");
		// textEmail.setText("scott.cheezem@gmail.com");

		buttonRegister.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				name = textName.getText().toString();
				email = textEmail.getText().toString();
				// Get GCM registration id
				// final String regId = GCMRegistrar.getRegistrationId(this);
				regId = GCMRegistrar.getRegistrationId(getActivity());

				if (regId.equals("")) {

					GCMRegistrar.register(getActivity(),
							new SavvyAPI().SENDER_ID);
					regId = GCMRegistrar.getRegistrationId(getActivity());

				}

				Log.v(TAG, "regId is " + regId);

				if (name.trim().length() > 0 && email.trim().length() > 0) {
					// launch/load the new post intent

					// send register parameters to backend

					// execute background task...

					new RegisterUserTask().execute();

				} else {
					// alert.showAlertDialog(RegisterFragment.this,"Registration Error! Try again");
					Log.v(TAG, "error registering, make this a real alert!");

				}
			}
		});
		// {

		// }

		return myFragmentView;
	}

	class RegisterUserTask extends SavvyAsyncTask<String, Integer, Boolean> {
		private int registerResult = 0;

		@Override
		protected void onPreExecute() {
			// show a loader
			super.onPreExecute(getActivity());

		}

		@Override
		protected void onPostExecute(Boolean result) {

			if (result) {
				prefs.edit().putString("name", name).commit();
				prefs.edit().putString("email", email).commit();
				// prefs.edit().putString("regId", regId).commit();
				prefs.edit().putInt("registered", registerResult).commit();
				// prefs.edit().commit();
				Log.v(TAG,
						"saved " + prefs.getString("name", "name") + ","
								+ prefs.getString("email", "email") + ","
								+ prefs.getString("regId", "regId") + ","
								+ prefs.getInt("registered", 0));

				transition();
			} else {
				new RegisterUserTask().execute();
			}
		}

		@Override
		protected Boolean doInBackground(String... arg0) {
			// TODO Auto-generated method stub

			try {

				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				regId = prefs.getString("regId", regId);

				if (regId.equals(null) || regId.equals("")) {
					return false;
				} else {

					Log.v(TAG, "Registering user with " + name + "," + email
							+ "," + regId);

					registerResult = new SavvyAPI().registerUser(name, email,
							regId);
				}

			} catch (ClientProtocolException e) {

				e.printStackTrace();
			} catch (IOException e) {

				e.printStackTrace();
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			return true;
		}
	}

	private void transition() {
		getFragmentManager().popBackStackImmediate();
	}
}
