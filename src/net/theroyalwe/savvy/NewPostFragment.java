package net.theroyalwe.savvy;

import java.io.IOException;
import java.util.ArrayList;

import net.theroyalwe.savvy.MainActivity.TabListener;

import org.apache.http.client.ClientProtocolException;
import org.json.JSONException;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.TextView;

import com.actionbarsherlock.app.SherlockFragment;
import com.actionbarsherlock.app.SherlockListFragment;

public class NewPostFragment extends SherlockFragment {
	private static String TAG = "User Update";

	AutoCompleteTextView categoryDropDown;
	EditText textQuantity;
	EditText textPrice;
	TextView addressText;
	RatingBar textRating;
	String userid;
	ReverseGeoCoding geoCoder = new ReverseGeoCoding();
	// ArrayList<SavvyCategory> categories = new ArrayList<SavvyCategory>();

	private LocationManager locationManager;
	private LocationListener locationListener;

	private Location currentLocation;
	int qty;
	float price;

	float rating;

	Button buttonSubmitPost;
	Button goToPurchaseHistory;
	SharedPreferences prefs;

	ArrayList<SavvyItem> responseItem = new ArrayList<SavvyItem>();

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		int registered = PreferenceManager.getDefaultSharedPreferences(getActivity()).getInt("registered", 0); 
		if(registered == 0){//user is not registered
			RegisterFragment registerFragment = new RegisterFragment();
			FragmentTransaction ft = getFragmentManager().beginTransaction();
			ft.replace(getId(), registerFragment);
			ft.setTransition(FragmentTransaction.TRANSIT_NONE);
			ft.addToBackStack(null);
			ft.commit();
		}
		super.onCreate(savedInstanceState);

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

		// prefs = getActivity().getSharedPreferences("pref", 0);
		// use prefs to get the user id.

		//hmm will we send a SavvyItem to the API or a bunch of string data..?
		
		
		View myFragmentView = inflater.inflate(R.layout.new_post,container, false);
		
		
		
		categoryDropDown = (AutoCompleteTextView)myFragmentView.findViewById(R.id.categoryDropDown);
		textQuantity = (EditText) myFragmentView.findViewById(R.id.editTextItemQty);
		textPrice = (EditText) myFragmentView.findViewById(R.id.editTextItemPrice);
		
		textRating = (RatingBar) myFragmentView.findViewById(R.id.itemPostRating);
		
		buttonSubmitPost = (Button) myFragmentView.findViewById(R.id.itemPostSubmitBtn);
		
		
		
		addressText = (TextView)myFragmentView.findViewById(R.id.address_text);
		
		goToPurchaseHistory = (Button)myFragmentView.findViewById(R.id.myPurchaseHistory);
		
		
		
		new GetCategoriesTask().execute();

		registerLocationListener();
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
		
		userid = Integer.toString(prefs.getInt("registered", 0));

		Log.v(TAG, "ready to post for user with id " + userid);

		buttonSubmitPost.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {

				new submitSavvyItemTask().execute();

			}
		});

		goToPurchaseHistory.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				unregisterLocationListener();
				goToPurchaseHistory();
			}

		});

		return myFragmentView;
	}
	
	
	private void updateUI(){
		  //SavvyCategory [] categoryArray = items.toArray(new SavvyCategory[items.size()]);
		  //CategoryAdapter adapter = new CategoryAdapter(getActivity(), R.layout.listview_item_row, categoryArray);
		Log.v(TAG, Integer.toString(MainActivity.categories.size()));
		String categoryNames[] = new String[MainActivity.categories.size()];

		for (int i = 0; i < MainActivity.categories.size(); i++) {
			categoryNames[i] = MainActivity.categories.get(i).name;
		}

		categoryDropDown.setAdapter(new ArrayAdapter<String>(getActivity(),
				android.R.layout.simple_dropdown_item_1line, categoryNames));

	}
	@Override
	public void onDestroy() {

		Log.v(TAG, "destroying");
		unregisterLocationListener();
		super.onDestroy();

	}
	@Override
	public void onPause() {
		super.onPause();
		Log.v(TAG, "pausing");
		unregisterLocationListener();
	}

	@Override
	public void onResume() {

		super.onResume();

		registerLocationListener();

	}

	private void registerLocationListener() {

		Log.v(TAG, "registereing new locationListener");
		// Acquire a reference to the system Location Manager
		locationManager = MainActivity.lm;
		// Define a listener that responds to location updates

		locationListener = new LocationListener() {

			public void onLocationChanged(Location location) {
				// Called when a new location is found by the location provider.
				
				Criteria criteria = new Criteria();
			    criteria.setAccuracy(Criteria.ACCURACY_COARSE);
			    String bestProvider = locationManager.getBestProvider(criteria, true);
			    Location l = null;
				if (bestProvider != null) l = locationManager.getLastKnownLocation(bestProvider);
				final boolean gpsEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
				//If GPS is not enabled, location was not retrieved, and the user hasn't previously ignored this dialog, prompt to enable GPS
			    if (!gpsEnabled && l == null && !PreferenceManager.getDefaultSharedPreferences(getActivity()).getBoolean("noGPS", false)) {
			        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
			        builder.setTitle("Location not enabled");
			        builder.setMessage("Savvy works best with access to your location.  Would you like to enable GPS?");
			        builder.setPositiveButton("Enable GPS", new DialogInterface.OnClickListener() {
						
						@Override
						public void onClick(DialogInterface dialog, int which) {
							enableLocationSettings();
							dialog.dismiss();
							
						}
					});
			        builder.setNegativeButton("Ignore", new DialogInterface.OnClickListener() {
						
						@Override
						public void onClick(DialogInterface dialog, int which) {
							PreferenceManager.getDefaultSharedPreferences(getActivity()).edit().putBoolean("noGPS", true);
							dialog.cancel();
						}
					});
			        builder.show();
			    }
				
				/*if(lastUpdate == null || distanceBetween(userLatLng, lastUpdate) > 50){
					new UpdateItemsTask().execute();
				}*/
				currentLocation = location;

				String address = geoCoder
						.getLocationAddressByLatitudeLongitude(location
								.getLatitude(), location.getLongitude(),
								getActivity().getApplicationContext());
				Log.v(TAG, "Currently Near :" + address);
				addressText.setText(address);

			}
			private void enableLocationSettings() {
			    Intent settingsIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
			    startActivity(settingsIntent);
			}

			public void onStatusChanged(String provider, int status,
					Bundle extras) {
			}

			public void onProviderEnabled(String provider) {
			}

			public void onProviderDisabled(String provider) {
			}
		};
		// Register the listener with the Location Manager to receive location
		// updates as frequently as possible
		if (locationManager.getAllProviders().contains(
				LocationManager.NETWORK_PROVIDER)) {
			locationManager.requestLocationUpdates(
					LocationManager.NETWORK_PROVIDER, 30000, 10,
					locationListener);
		}
		if (locationManager.getAllProviders().contains(
				LocationManager.GPS_PROVIDER)) {
			locationManager.requestLocationUpdates(
					LocationManager.GPS_PROVIDER, 30000, 10, locationListener);
		}
	}
	private void unregisterLocationListener(){
		Log.v(TAG, "unregistereing locationListener");
		if(locationManager != null){
			locationManager.removeUpdates(locationListener);
		}
	}

	private class GetCategoriesTask extends
			SavvyAsyncTask<String, Integer, Boolean> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute(getActivity());
		}

		@Override
		protected void onPostExecute(Boolean result) {
			// update The list, update UI
			updateUI();
		}

		@Override
		protected Boolean doInBackground(String... args) {
			Log.v(TAG, "in Background doing stuff");
			// make the http call

			try {
				if (MainActivity.categories.size() == 0) {
					MainActivity.categories = new SavvyAPI().getCategories();
				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ClientProtocolException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			return null;
		}
	}

	
	public void goToItemInfoFragment(SavvyItem item){
		
        Bundle bundle = new Bundle();
        bundle.putParcelable(getString(R.string.ITEM_KEY), item);
        
        SherlockFragment itemInfoFragment = new ItemInfoFragment();
        itemInfoFragment.setArguments(bundle);
        
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        
        
        ft.replace(getId(), itemInfoFragment);
        //ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        ft.addToBackStack(null);
        ft.commit();
	}
	
	public void goToPurchaseHistory(){
		Bundle bundle = new Bundle();
		bundle.putString("userid", userid);

		SherlockListFragment purchaseHistory = new CategoryListFragment();
		purchaseHistory.setArguments(bundle);

		FragmentTransaction ft = getFragmentManager().beginTransaction();

		ft.replace(getId(), purchaseHistory);
		ft.addToBackStack(null);
		ft.commit();

	}

	private class submitSavvyItemTask extends
			SavvyAsyncTask<String, Integer, Boolean> {

		ProgressDialog dialog = new ProgressDialog(getActivity());

		@Override
		protected void onPreExecute() {
			// show a loader
			super.onPreExecute(getActivity());

			dialog.setMessage("Transmitting...");
			dialog.show();

		}

		@Override
		protected void onPostExecute(Boolean result) {
			// update The list, update UI

			// show a message to the user

			// go to a new itemview with the new users item?
			unregisterLocationListener();

			if (dialog.isShowing()) {
				dialog.dismiss();
			}
			if (responseItem.size() > 0) {

				categoryDropDown.setText("");
				textQuantity.setText("");

				textPrice.setText("");
				textRating.setRating(0);

				goToItemInfoFragment(responseItem.get(0));

			}

		}

		@Override
		protected Boolean doInBackground(String... params) {
			// TODO Auto-generated method stub

			String lat = Double.toString(currentLocation.getLatitude());
			String lon = Double.toString(currentLocation.getLongitude());
			String itemName = categoryDropDown.getText().toString();
			String price = textPrice.getText().toString();
			String quantity = textQuantity.getText().toString();
			String rating = Float.toString(textRating.getRating());
			// String userid = Integer.toString(prefs.getInt("registered", 0));
			String catid = "";
			Log.v(TAG, "looking up item " + itemName);
			if (!itemName.trim().equals("")) {
				for (SavvyCategory c : MainActivity.categories) {
					// for(int i = 0 ; i < MainActivity.categories.size() ; i++)
					Log.v(TAG, c.getName());
					if (c.getName().equals(itemName)) {
						Log.v(TAG, "Found category:" + c.getName());
						catid = Integer.toString(c.getId());
						break;
					}
				}

				try {
					
					responseItem = new SavvyAPI().postSavvyItem(catid, lat, lon, userid, price, quantity, rating);
					
				} catch (ClientProtocolException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

			return null;
		}

	}
}
