package net.theroyalwe.savvy;

import java.util.Locale;

import android.R;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.ListFragment;
import android.util.Log;

/**
 * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
 * one of the sections/tabs/pages.
 */
public class SectionsPagerAdapter extends FragmentPagerAdapter {
	Context mApplicationContext;
public static String TAG = "Pager";
	public SectionsPagerAdapter(FragmentManager fm, Context context) {
		super(fm);
		this.mApplicationContext = context;
	}

	@Override
	public Fragment getItem(int position) {
		// getItem is called to instantiate the fragment for the given page.
		// Return a DummySectionFragment (defined as a static inner class
		// below) with the page number as its lone argument.
		
		
		if(position == 0){
			ListFragment fragment = new CategoryListFragment();
			return fragment;
		}else if (position == 2){
			Log.v(TAG, "at position "+Integer.toString(position));
			//check a persistant preference
			Fragment fragment = new RegisterFragment();
			return fragment;
		}else{
			
		
		
			Fragment fragment = new DummySectionFragment();
			Bundle args = new Bundle();
			args.putInt(DummySectionFragment.ARG_SECTION_NUMBER, position + 1);
			fragment.setArguments(args);
			return fragment;
		}
		
		
	}

	@Override
	public int getCount() {
		// Show 3 total pages.
		return 3;
	}

	@Override
	public CharSequence getPageTitle(int position) {
		Locale l = Locale.getDefault(); 
		switch (position) {
		case 0:
			return "Categories";//getString(R.string.title_section1).toUpperCase(l);
		case 1:
			return "Foo";//mApplicationContext.getString(R.string.title_section2).toUpperCase(l);
		case 2:
			return "Register User";//mApplicationContext.getString(R.string.title_section3).toUpperCase(l);
		}
		return null;
	}
}
