package net.theroyalwe.savvy;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.preference.PreferenceManager;
import android.util.Log;

import com.google.android.gcm.GCMBaseIntentService;

public class GCMIntentService extends com.google.android.gcm.GCMBaseIntentService{
	
	private static String TAG = "GCMIntentService";

	DisplayMessageUtils disp = new DisplayMessageUtils();
	
	@Override
	protected void onError(Context context, String error) {
		// TODO Auto-generated method stub
		Log.v(TAG, "There was an error Registering : "+error);
		
	}

	@Override
	protected void onMessage(Context arg0, Intent intent) {
		// TODO Auto-generated method stub
		
		
		String message = intent.getExtras().getString("message");
		Log.v(TAG, "Recieved Push message "+message);
		DisplayMessageUtils.displayMessage(arg0, "message");
		generateNotification(arg0, message);
		//generate a Notification?
		
	}

	@Override
	protected void onRegistered(Context arg0, String regId) {
		// TODO Auto-generated method stub
		Log.v(TAG, "Device was registered with "+regId);
		PreferenceManager.getDefaultSharedPreferences(arg0).edit().putString("regId", regId).commit();
		
		//the call the register api here, not sure if thats the best way though.  though this might be the best place to save the prefs?
		
	}

	@Override
	protected void onUnregistered(Context arg0, String regId) {
		// TODO Auto-generated method stub
		Log.v(TAG, "be sure to call the unregister api with "+regId);
	}
	

	
	private static void generateNotification(Context context, String message) {
        int icon = R.drawable.ic_launcher;
        long when = System.currentTimeMillis();
        NotificationManager notificationManager = (NotificationManager)
                context.getSystemService(Context.NOTIFICATION_SERVICE);
        Notification notification = new Notification(icon, message, when);
        
        String title = context.getString(R.string.app_name);
        
        Intent notificationIntent = new Intent(context, MainActivity.class);
        // set intent so it does not start a new activity
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
                Intent.FLAG_ACTIVITY_SINGLE_TOP);
        PendingIntent intent =
                PendingIntent.getActivity(context, 0, notificationIntent, 0);
        notification.setLatestEventInfo(context, title, message, intent);
        notification.flags |= Notification.FLAG_AUTO_CANCEL;
        
        // Play default notification sound
        notification.defaults |= Notification.DEFAULT_SOUND;
        
        //notification.sound = Uri.parse("android.resource://" + context.getPackageName() + "your_sound_file_name.mp3");
        
        // Vibrate if vibrate is enabled
        notification.defaults |= Notification.DEFAULT_VIBRATE;
        notificationManager.notify(0, notification);      

    }
	

}
