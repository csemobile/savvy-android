package net.theroyalwe.savvy;


import java.io.IOException;

import org.apache.http.client.ClientProtocolException;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.actionbarsherlock.app.SherlockFragment;

public class UserFragment extends SherlockFragment {
	private static String TAG = "User Update";
	
	EditText textName;
	EditText textEmail;
	EditText textAddress;
	EditText textZip;
	
	String name;
	String email;
	String address;
	String zipCode;
	
	Button buttonUserUpdate;
	Button buttonNewPost;
	SharedPreferences prefs;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

		prefs = getActivity().getSharedPreferences("pref", 0);

		View myFragmentView = inflater.inflate(R.layout.user_details,
				container, false);
		
		textName = (EditText) myFragmentView.findViewById(R.id.editUserName);
		textEmail = (EditText) myFragmentView.findViewById(R.id.editTextUserEmail);
		textAddress = (EditText) myFragmentView.findViewById(R.id.editUserAddress);
		textZip = (EditText) myFragmentView.findViewById(R.id.editUserZip);
		
		buttonUserUpdate = (Button) myFragmentView.findViewById(R.id.buttonUserUpdate);
		buttonNewPost = (Button) myFragmentView.findViewById(R.id.buttonUserPost);
		if (prefs.getString("name", "") != null) {

			textName.setText(prefs.getString("name", ""));
		}
		if (prefs.getString("email", "") != null) {
			textEmail.setText(prefs.getString("email", ""));
		}
		if (prefs.getString("address", "") != null) {

			textAddress.setText(prefs.getString("address", ""));
		}
		if (prefs.getString("zip", "") != null) {
			
			textZip.setText(prefs.getString("zip", ""));
		}
		
		/*buttonUserUpdate.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				name = textName.getText().toString();
				email = textEmail.getText().toString();
				address = textAddress.getText().toString();
				zipCode = textZip.getText().toString();
				if (name.trim().length() > 0 && email.trim().length() > 0 && zipCode.trim().length() > 0) {
					SavvyUser u = new SavvyUser();
					u.setAddress(address);
					u.setEmail(email);
					u.setFullName(name);
					u.setZipCode(zipCode);
					try {

						Log.v(TAG, "Updating user");

						new SavvyAPI().updateUser(u);
						
					} catch (ClientProtocolException e) {

						e.printStackTrace();
					} catch (IOException e) {

						e.printStackTrace();
					}


				} else {
					Log.v(TAG, "error registering, make this a real alert!");

				}
				
				
			}
		});	*/
		
		buttonNewPost.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				
			}
		});	
		
		return myFragmentView;
	}
	
		
}
