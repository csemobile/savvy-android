package net.theroyalwe.savvy;

import java.text.NumberFormat;

import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.TextView;

import com.actionbarsherlock.app.SherlockFragment;

public class ItemInfoFragment extends SherlockFragment{
	
	SavvyItem mitem;
	
	@Override
	public void onCreate (Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		SavvyItem itemParceable = (SavvyItem) getArguments().getParcelable(getString(R.string.ITEM_KEY));
		mitem = itemParceable;
	}
	
	@Override
	public View onCreateView (LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
		return inflater.inflate(R.layout.item_info_view, container,false);
	}
	
	@Override
	public void onDestroy(){
		super.onDestroy();
	}
	@Override
	public void onPause(){
		super.onPause();
	}
	public void onResume(){
		super.onResume();
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
	    super.onActivityCreated(savedInstanceState); 
	    TextView title = (TextView) getView().findViewById(R.id.itemInfoTitle);
	    title.setText(mitem.getName());
	    
	    TextView price = (TextView) getView().findViewById(R.id.itemInfoPrice);
	    price.setText(toMoneyString(mitem.getPrice()));
	    
	    RatingBar ratingBar = (RatingBar) getView().findViewById(R.id.itemInfoRatingBar);
	    ratingBar.setRating(mitem.getRating());
	    
	    EditText itemAddress = (EditText)getView().findViewById(R.id.itemAddress);
	    ReverseGeoCoding r = new ReverseGeoCoding();
	    String addr = r.getLocationAddressByLatitudeLongitude(mitem.getLat(),mitem.getLon(), getActivity());
	    itemAddress.setText(addr);
	    
	    final Button goToMapButton = (Button) getView().findViewById(R.id.itemInfoMapViewButton);
        goToMapButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	ItemMap itemMap = new ItemMap();
            	Bundle bundle = new Bundle();
            	
      		  	bundle.putParcelable(getString(R.string.ITEM_KEY), mitem);
      		  	itemMap.setArguments(bundle);
            	
            	FragmentTransaction ft = getFragmentManager().beginTransaction();
                ft.replace(getId(), itemMap);
                ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
                ft.addToBackStack(null);
                ft.commit();
            }
        });
	  }

	private CharSequence toMoneyString(double price) {
		NumberFormat formatter = NumberFormat.getCurrencyInstance();
		return formatter.format(price);
	}

	 	  
	  
}

