package net.theroyalwe.savvy;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;

public class ReverseGeoCoding// extends Activity 
{
	public String getLocationAddressByLatitudeLongitude(double LATITUDE, double LONGITUDE, Context context)
	{
		String address = null;
		// can use Mapview
		/*
		 * GeoPoint p = mapView.getProjection().fromPixels((int) event.getX(),(int) event.getY());
		 */
		//Geocoder geocoder = new Geocoder(getBaseContext(),Locale.ENGLISH);
		Geocoder geocoder = new Geocoder(context,Locale.ENGLISH);
		try {
			List<Address> addresses = geocoder.getFromLocation(LATITUDE, LONGITUDE, 1);

/*			List<Address> addresses = geoCoder.getFromLocation(
                    p.getLatitudeE6()  / 1E6, 
                    p.getLongitudeE6() / 1E6, 1);*/
			if(addresses != null) {
				Address returnedAddress = addresses.get(0);
				StringBuilder strReturnedAddress = new StringBuilder("Near\n");
				for(int i=0; i<returnedAddress.getMaxAddressLineIndex(); i++) {
					strReturnedAddress.append(returnedAddress.getAddressLine(i)).append("\n");
				}
				address = strReturnedAddress.toString();
			}
			else{
				address =  "No Address returned!";
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			address = "Canont get Address!";
		}
 
		return address;
	}
}
