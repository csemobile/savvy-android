package net.theroyalwe.savvy;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;

abstract class SavvyAsyncTask<Params, Progress, Result> extends
		AsyncTask<Params, Progress, Result> {

	protected void onPreExecute(Context activity) {
		if (!isNetworkOnline(activity.getApplicationContext())) {
			displayDialog(activity);
		}
	}

	public boolean isNetworkOnline(Context context) {
		boolean status = false;
		try {
			ConnectivityManager cm = (ConnectivityManager) context
					.getSystemService(Context.CONNECTIVITY_SERVICE);
			NetworkInfo netInfo = cm.getNetworkInfo(0);
			if (netInfo != null
					&& netInfo.getState() == NetworkInfo.State.CONNECTED) {
				status = true;
			} else {
				netInfo = cm.getNetworkInfo(1);
				if (netInfo != null
						&& netInfo.getState() == NetworkInfo.State.CONNECTED)
					status = true;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return status;

	}

	public void displayDialog(final Context activity) {
		AlertDialog.Builder builder = new AlertDialog.Builder(activity);
		builder.setMessage(R.string.dialog_message).setTitle(
				R.string.dialog_title);
		builder.setPositiveButton("Connection Settings",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						activity.startActivity(new Intent(
								WifiManager.ACTION_PICK_WIFI_NETWORK));
					}
				});

		AlertDialog dialog = builder.create();
		dialog.show();
	}
}
