package net.theroyalwe.savvy;

public class SavvyUser {
String fullName;
String email;
String address;
String zipCode;
public String getUserId() {
	return userId;
}
public void setUserId(String userId) {
	this.userId = userId;
}
String userId;
public String getFullName() {
	return fullName;
}
public void setFullName(String fullName) {
	this.fullName = fullName;
}
public String getEmail() {
	return email;
}
public void setEmail(String email) {
	this.email = email;
}
public String getAddress() {
	return address;
}
public void setAddress(String address) {
	this.address = address;
}
public String getZipCode() {
	return zipCode;
}
public void setZipCode(String zipCode) {
	this.zipCode = zipCode;
}
}
