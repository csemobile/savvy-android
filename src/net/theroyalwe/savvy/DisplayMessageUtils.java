package net.theroyalwe.savvy;


import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;



public class DisplayMessageUtils {
	
	
	
	
		
		// give your server registration url here
	    static final String SERVER_URL = "http://savvy.theroyalwe.net/gcm/register.php";

	    // Google project id
	    static final String SENDER_ID = "800377911519";

	    /**
	     * Tag used on log messages.
	     */
	    static final String TAG = "AndroidHive GCM";

	    static final String DISPLAY_MESSAGE_ACTION = "net.throyalwe.net.savvy.DISPLAY_MESSAGE";

	    static final String EXTRA_MESSAGE = "message";

	    /**
	     * Notifies UI to display a message.
	     * <p>
	     * This method is defined in the common helper because it's used both by
	     * the UI and the background service.
	     *
	     * @param context application's context.
	     * @param message message to be displayed.
	     */
	    static void displayMessage(Context context, String message) {
	    	Log.v("DISP", context.toString());
	        Intent intent = new Intent(DISPLAY_MESSAGE_ACTION);
	        intent.putExtra(EXTRA_MESSAGE, message);
	        context.sendBroadcast(intent);
	        
	        Toast.makeText(context, "New Message: " + message, Toast.LENGTH_LONG).show();
	        
	    }
	

	
	

}
