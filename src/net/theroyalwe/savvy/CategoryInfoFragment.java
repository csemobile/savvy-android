package net.theroyalwe.savvy;

import java.io.IOException;
import java.util.ArrayList;

import org.apache.http.client.ClientProtocolException;
import org.json.JSONException;

import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.actionbarsherlock.app.SherlockFragment;

public class CategoryInfoFragment extends SherlockFragment {

	private static final String TAG = "CATEGORY_INFO_FRAGMENT";

	SavvyCategory mCategory;
	ArrayList<SavvyItem> items;

	private boolean itemArrayCreated = false;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mCategory = (SavvyCategory) getArguments().get(
				getString(R.string.CATEGORY_KEY));
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
	}

	@Override
	public void onPause() {
		super.onPause();
	}

	public void onResume() {
		super.onResume();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		return inflater.inflate(R.layout.category_view, container, false);
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		TextView title = (TextView) getView().findViewById(
				R.id.categoryInfoTitle);
		title.setText(mCategory.getName());

		ImageView image = (ImageView) getView().findViewById(
				R.id.categoryInfoImage);
		image.setImageBitmap(mCategory.getImage());

		if (getArguments().containsKey("useritems")) {
			items = getArguments().getParcelableArrayList("useritems");
			itemArrayCreated = true;
		}

		if (!itemArrayCreated) {
			new GetItemsTask().execute();
		} else {
			updateUI();
		}
	}

	public void updateUI() {
		if (items != null) {
			ListView listView = (ListView) getView().findViewById(
					R.id.categoryInfoListView);
			SavvyItem[] data = items.toArray(new SavvyItem[items.size()]);
			ItemListAdapter adapter = new ItemListAdapter(getActivity(),
					R.layout.listview_item_row, data, mCategory);

			listView.setOnItemClickListener(new OnItemClickListener() {
				@Override
				public void onItemClick(AdapterView<?> parent, View view,
						int position, long id) {
					SavvyItem item = (SavvyItem) parent
							.getItemAtPosition(position);
					ItemInfoFragment itemInfoFragment = new ItemInfoFragment();
					Bundle bundle = new Bundle();
					bundle.putParcelable(getString(R.string.ITEM_KEY), item);
					itemInfoFragment.setArguments(bundle);
					FragmentTransaction ft = getFragmentManager()
							.beginTransaction();
					ft.replace(getId(), itemInfoFragment);
					ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
					ft.addToBackStack(null);
					ft.commit();

				}
			});
			listView.setAdapter(adapter);
			itemArrayCreated = true;
		}

	}

	private class GetItemsTask extends SavvyAsyncTask<String, Integer, Boolean> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute(getActivity());
		}

		@Override
		protected void onPostExecute(Boolean result) {
			// update The list, update UI
			updateUI();
		}

		@Override
		protected Boolean doInBackground(String... args) {
			Log.v(TAG, "in Background doing stuff");
			// make the http call

			try {

				items = new SavvyAPI()
						.getItemsForCategoryByCategoryId(mCategory.getId());
			} catch (JSONException e) {
				for (StackTraceElement element : e.getStackTrace()) {
					Log.e(TAG, element.toString());
				}
			} catch (ClientProtocolException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			return true;
		}

	}

}
