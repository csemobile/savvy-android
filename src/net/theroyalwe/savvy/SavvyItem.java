package net.theroyalwe.savvy;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import android.os.Parcel;
import android.os.Parcelable;

public class SavvyItem implements Parcelable{
	int id;
	float lat;
	float lon;
	int catId;
	int userId;
	double price;
	int quantity;
	float rating;
	Date ts;
	SimpleDateFormat dateFormat = new SimpleDateFormat("MM-dd-yyyy hh:mm:ss", Locale.US);
	String name;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public float getLat() {
		return lat;
	}
	public void setLat(float lat) {
		this.lat = lat;
	}
	public float getLon() {
		return lon;
	}
	public void setLon(float lon) {
		this.lon = lon;
	}
	public int getCatId() {
		return catId;
	}
	public void setCatId(int catId) {
		this.catId = catId;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Date getTs() {
		return ts;
	}
	public void setTs(String string) {
		
		this.ts = new Date();
		try{
			
			this.ts = this.dateFormat.parse(string);
		}catch (java.text.ParseException e){
			e.printStackTrace();
		} 
		
	}
	public float getRating() {
		return rating;
	}
	public void setRating(float rating) {
		this.rating = rating;
	}
	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}
	@Override
	public void writeToParcel(Parcel dest, int flags) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public int hashCode(){
		return (int) (this.name.hashCode() % this.lon);
		
	}
	@Override
	public boolean equals(Object obj){
		if (obj == null)
            return false;
        if (obj == this)
            return true;
        if (obj.getClass() != getClass())
            return false;
        SavvyItem i = (SavvyItem)obj;
        return this.id == i.id;
	}
	

}
