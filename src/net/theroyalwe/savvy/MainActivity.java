package net.theroyalwe.savvy;

import static net.theroyalwe.savvy.DisplayMessageUtils.EXTRA_MESSAGE;

import java.util.ArrayList;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.widget.Toast;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.ActionBar.Tab;
import com.actionbarsherlock.app.SherlockFragmentActivity;

public class MainActivity extends SherlockFragmentActivity {

	/**
	 * The {@link android.support.v4.view.PagerAdapter} that will provide
	 * fragments for each of the sections. We use a
	 * {@link android.support.v4.app.FragmentPagerAdapter} derivative, which
	 * will keep every loaded fragment in memory. If this becomes too memory
	 * intensive, it may be best to switch to a
	 * {@link android.support.v4.app.Fr`agmentStatePagerAdapter}.
	 */
	SectionsPagerAdapter mSectionsPagerAdapter;
	ActionBar bar;
	public static LocationManager lm;

	public static ArrayList<SavvyCategory> categories = new ArrayList<SavvyCategory>();

	/**
	 * The {@link ViewPager} that will host the section contents.
	 */
	ViewPager mViewPager;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		lm = (LocationManager) getApplication().getSystemService(
				Context.LOCATION_SERVICE);
		categories.clear();

		setContentView(R.layout.activity_main);

		// Create the adapter that will return a fragment for each of the three
		// primary sections of the app.
		mSectionsPagerAdapter = new SectionsPagerAdapter(
				getSupportFragmentManager(), getApplicationContext());
		bar = getSupportActionBar();
		bar.setDisplayShowHomeEnabled(false);
		bar.setDisplayShowTitleEnabled(false);
		bar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
		Tab categoryTab = bar.newTab().setText("Categories");
		Tab mapTab = bar.newTab().setText("Map");
		Tab postTab = bar.newTab().setText("New Post");

		// Assign tab listeners to tabs
		postTab.setTabListener(new TabListener<NewPostFragment>(this,
				"register_tab", NewPostFragment.class));
		categoryTab.setTabListener(new TabListener<CategoryListFragment>(this,
				"cagtegory_tab", CategoryListFragment.class));
		mapTab.setTabListener(new TabListener<SavvyMapFragment>(this,
				"map_tab", SavvyMapFragment.class));

		bar.addTab(postTab);
		bar.addTab(categoryTab);
		bar.addTab(mapTab);

		/*
		 * if(registered == 0 ){ bar.selectTab(postTab); } else {
		 * bar.selectTab(categoryTab); }
		 */

		// Set up the ViewPager with the sections adapter.
		// mViewPager = (ViewPager) findViewById(R.id.pager);
		// mViewPager.setAdapter(mSectionsPagerAdapter);

	}

	/*
	 * @Override public boolean onCreateOptionsMenu(Menu menu) { // Inflate the
	 * menu; this adds items to the action bar if it is present.
	 * getMenuInflater().inflate(R.menu.main, menu); return true; }
	 */

	@Override
	public void onDestroy() {
		super.onDestroy();
	}

	@Override
	public void onPause() {
		super.onPause();
	}

	public void onResume() {
		super.onResume();
		// portrait application
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
	}

	public static class TabListener<T extends Fragment> implements
			ActionBar.TabListener {
		private final MainActivity mActivity;
		private final String mTag;
		private final Class<T> mClass;
		private final Bundle mArgs;
		private Fragment mFragment;

		public TabListener(MainActivity activity, String tag, Class<T> clz) {
			this(activity, tag, clz, null);
		}

		public TabListener(MainActivity activity, String tag, Class<T> clz,
				Bundle args) {
			mActivity = activity;
			mTag = tag;
			mClass = clz;
			mArgs = args;

			// Check to see if we already have a fragment for this tab, probably
			// from a previously saved state. If so, deactivate it, because our
			// initial state is that a tab isn't shown.
			mFragment = mActivity.getSupportFragmentManager()
					.findFragmentByTag(mTag);
			if (mFragment != null && !mFragment.isDetached()) {
				FragmentTransaction ft = mActivity.getSupportFragmentManager()
						.beginTransaction();
				ft.detach(mFragment);
				ft.commit();
			}
		}

		public void onTabSelected(Tab tab, FragmentTransaction ft) {
			mFragment = mActivity.getSupportFragmentManager()
					.findFragmentByTag(mTag);

			if (mFragment == null) {
				mFragment = Fragment.instantiate(mActivity, mClass.getName(),
						mArgs);
				ft.add(android.R.id.content, mFragment, mTag);
			} else {
				ft.attach(mFragment);
			}
		}

		public void onTabUnselected(Tab tab, FragmentTransaction ft) {
			if (mFragment != null) {
				FragmentManager fm = mFragment.getFragmentManager();
				while (fm.getBackStackEntryCount() > 0) {
					fm.popBackStackImmediate();
				}
				ft.detach(mFragment);
			}
		}

		public void onTabReselected(Tab tab, FragmentTransaction ft) {
			// Toast.makeText(mActivity, "Reselected!",
			// Toast.LENGTH_SHORT).show();
		}
	}

	/**
	 * Receiving push messages was private
	 * */
	public final static BroadcastReceiver mHandleMessageReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			String newMessage = intent.getExtras().getString(EXTRA_MESSAGE);
			// Waking up mobile if it is sleeping
			WakeLocker.acquire(context);

			/**
			 * Take appropriate action on this message depending upon your app
			 * requirement For now i am just displaying it on the screen
			 * */

			// Showing received message
			// lblMessage.append(newMessage + "\n");
			Toast.makeText(context, "New Message: " + newMessage,
					Toast.LENGTH_LONG).show();

			// Releasing wake lock
			WakeLocker.release();
		}

	};

}
