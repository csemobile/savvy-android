package net.theroyalwe.savvy;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.http.client.ClientProtocolException;
import org.json.JSONException;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnInfoWindowClickListener;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

public class SavvyMapFragment extends SupportMapFragment {

	private LocationManager locationManager;
	private LocationListener locationListener;
	private String bestProvider;
	private LatLng userLatLng;
	private LatLng lastUpdate = null;
	private GoogleMap map = null;

	private ArrayList<SavvyItem> items = new ArrayList<SavvyItem>();
	private static final String TAG = "MAP";
	private boolean initialized = false;
	
	/**
	 * Create a new instance of MapFragment, initialized to show the location
	 * Entry at 'index'.
	 */
	public static SavvyMapFragment newInstance(int index) {
		SavvyMapFragment f = new SavvyMapFragment();

		// Supply index input as an argument.
		Bundle args = new Bundle();
		args.putInt("index", index);
		f.setArguments(args);

		return f;
	}
	//return distance between two points in meters
	public double distanceBetween(LatLng l1, LatLng l2){
		double lat1 = l1.latitude, lat2 = l2.latitude, lng1 = l1.longitude, lng2 = l2.longitude;
		double earthRadius = 3958.75;
		double dLat = Math.toRadians(lat2 - lat1);
		double dLng = Math.toRadians(lng2 - lng1);
		double sindLat = Math.sin(dLat / 2);
		double sindLng = Math.sin(dLng / 2);
		double a = Math.pow(sindLat, 2) + Math.pow(sindLng, 2)
				* Math.cos(Math.toRadians(lat1))
				* Math.cos(Math.toRadians(lat2));
		double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
		double dist = earthRadius * c;
		int distance = (int) (dist * 1609.34);
		Log.v(TAG, "distance was " + Integer.toString(distance));
		return distance;
	}
	HashMap<Marker, SavvyItem> itemMarkers = new HashMap<Marker, SavvyItem>();
	Marker myMarker;
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		registerLocationListener();
		Criteria criteria = new Criteria();
	    criteria.setAccuracy(Criteria.ACCURACY_COARSE);
	    String bestProvider = locationManager.getBestProvider(criteria, true);
	    Location l = null;
		if (bestProvider != null) l = locationManager.getLastKnownLocation(bestProvider);
		final boolean gpsEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
		//If GPS is not enabled, location was not retrieved, and the user hasn't previously ignored this dialog, prompt to enable GPS
	    if (!gpsEnabled && l == null && !PreferenceManager.getDefaultSharedPreferences(getActivity()).getBoolean("noGPS", false)) {
	        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
	        builder.setTitle("Location not enabled");
	        builder.setMessage("Savvy works best with access to your location.  Would you like to enable GPS?");
	        builder.setPositiveButton("Enable GPS", new DialogInterface.OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					enableLocationSettings();
					dialog.dismiss();
					
				}
			});
	        builder.setNegativeButton("Ignore", new DialogInterface.OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					PreferenceManager.getDefaultSharedPreferences(getActivity()).edit().putBoolean("noGPS", true);
					dialog.cancel();
				}
			});
	        builder.show();
	    }
		map = this.getMap();

		map.setMyLocationEnabled(true);
		//if the map is just now initialized and we have a location, zoom to their location
		if(!initialized && l!=null){
		map.moveCamera(CameraUpdateFactory.newLatLng(new LatLng(l.getLatitude(), l.getLongitude())));
		map.moveCamera(CameraUpdateFactory.zoomTo(16));
		initialized = true;
		}

		Log.d("MAP", "Map created");

	}

	private void enableLocationSettings() {
	    Intent settingsIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
	    startActivity(settingsIntent);
	}
	
	@Override
	public void onDestroy() {

		Log.v(TAG,  "destroying");
		//stop getting updates
		locationManager.removeUpdates(locationListener);
		map.setMyLocationEnabled(false);

		super.onDestroy();

	}

	@Override
	public void onPause() {
		super.onPause();
		Log.v(TAG, "pausing");
		locationManager.removeUpdates(locationListener);
	}

	@Override
	public void onResume() {

		super.onResume();

		map.setOnInfoWindowClickListener(new infoWindowClickListener());
		registerLocationListener();

	}

	private void registerLocationListener() {

		// Acquire a reference to the system Location Manager
		locationManager = MainActivity.lm;
		// Define a listener that responds to location updates

		locationListener = new LocationListener() {

			public void onLocationChanged(Location location) {
				// Called when a new location is found by the location provider.

				double lat = location.getLatitude();
				double lon = location.getLongitude();
				userLatLng = new LatLng(lat, lon);
				
				if(lastUpdate == null || distanceBetween(userLatLng, lastUpdate) > 50){
					new UpdateItemsTask().execute();
					lastUpdate = userLatLng;
				}
				
				
				if(myMarker != null) myMarker.remove();
				
				//myMarker = map.addMarker(new MarkerOptions().title("You").position(userLatLng).icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_launcher)));
				android.util.Log.v("MAP", "Location changed");
			}

			public void onStatusChanged(String provider, int status,
					Bundle extras) {
			}

			public void onProviderEnabled(String provider) {
			}

			public void onProviderDisabled(String provider) {
			}
		};
		// Register the listener with the Location Manager to receive location
		// updates as frequently as possible
		if (locationManager.getAllProviders().contains(
				LocationManager.NETWORK_PROVIDER)) {
			locationManager.requestLocationUpdates(
					LocationManager.NETWORK_PROVIDER, 0, 3, locationListener);
		}
		if (locationManager.getAllProviders().contains(
				LocationManager.GPS_PROVIDER)) {
			locationManager.requestLocationUpdates(
					LocationManager.GPS_PROVIDER, 0, 3, locationListener);
		}
	}

	private class infoWindowClickListener implements OnInfoWindowClickListener {

		@Override
		public void onInfoWindowClick(Marker marker) {
			if (marker.getSnippet().equals("item")) {
				SavvyItem item = itemMarkers.get(marker);
				Log.i(TAG, "Clicked " + item.id + ": " + item.name);
				//Display the item info fragment for the item that was clicked
	               ItemInfoFragment itemInfoFragment = new ItemInfoFragment();
	               Bundle bundle = new Bundle();
	               bundle.putParcelable(getString(R.string.ITEM_KEY), item);
	               itemInfoFragment.setArguments(bundle);
	               FragmentTransaction ft = getFragmentManager().beginTransaction();
	               ft.replace(getId(), itemInfoFragment);
	               ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
	               ft.addToBackStack(null);
	               ft.commit();
			}

		}

	}

	private class UpdateItemsTask extends
			SavvyAsyncTask<String, Integer, Boolean> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute(getActivity());
		}

		@Override
		protected void onPostExecute(Boolean result) {
			Log.v(TAG, "finished query, updating userLoc");
			Log.v(TAG, "items now has " + Integer.toString(items.size()));
			for (SavvyItem item : items) {
				itemMarkers.put(map.addMarker(new MarkerOptions()
						.title(item.name)
						.position(new LatLng(item.lat, item.lon))
						.snippet("item")
						.icon(BitmapDescriptorFactory
								.fromResource(R.drawable.ic_launcher))), item);
			}

			lastUpdate = userLatLng;
		}

		@Override
		protected Boolean doInBackground(String... params) {
			try {
				Log.v(TAG, "sending "+Double.toString(userLatLng.latitude)+":"+Double.toString(userLatLng.longitude));
				//new SavvyAPI().findItemsInRadiusOfLatLon((float)userLatLng.latitude, (float)userLatLng.longitude, (double)10000);
				items.addAll(new SavvyAPI().findItemsInRadiusOfLatLon((float)userLatLng.latitude, (float)userLatLng.longitude, (double)10000));
				
			} catch (ClientProtocolException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return null;
		}

	}
}
