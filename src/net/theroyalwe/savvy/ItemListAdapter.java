package net.theroyalwe.savvy;

import java.io.InputStream;
import java.text.NumberFormat;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class ItemListAdapter extends ArrayAdapter<SavvyItem> {

	Context activity;
	int layoutResourceId;
	SavvyItem data[] = null;
	SavvyCategory category;

	public ItemListAdapter(Context activity, int layoutResourceId,
			SavvyItem[] data, SavvyCategory category) {
		super(activity, layoutResourceId, data);
		this.layoutResourceId = layoutResourceId;
		this.activity = activity;
		this.data = data;
		this.category = category;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View row = convertView;
		ItemHolder holder = null;

		if (row == null) {
			LayoutInflater inflater = ((Activity) activity).getLayoutInflater();
			row = inflater.inflate(layoutResourceId, parent, false);

			holder = new ItemHolder();
			holder.imgIcon = (ImageView) row.findViewById(R.id.imgIcon);
			holder.txtTitle = (TextView) row.findViewById(R.id.txtTitle);

			row.setTag(holder);
		} else {
			holder = (ItemHolder) row.getTag();
		}

		SavvyItem item = data[position];
		holder.txtTitle.setText(" " + toMoneyString(item.getPrice()));
		// Asynchronously load image from the url

		return row;
	}

	private CharSequence toMoneyString(double price) {
		NumberFormat formatter = NumberFormat.getCurrencyInstance();
		return formatter.format(price);
	}

	private class DownloadImageTask extends
			SavvyAsyncTask<String, Void, Bitmap> {
		ImageView bmImage;
		SavvyCategory category;

		@Override
		protected void onPreExecute() {
			super.onPreExecute(getContext());
		}

		public DownloadImageTask(ImageView bmImage,
				SavvyCategory categoryToUpdate) {
			this.bmImage = bmImage;
			this.category = categoryToUpdate;
		}

		protected Bitmap doInBackground(String... urls) {
			String urldisplay = urls[0];
			Bitmap mIcon11 = null;
			try {
				InputStream in = new java.net.URL(urldisplay).openStream();
				mIcon11 = BitmapFactory.decodeStream(in);
			} catch (Exception e) {
				Log.e("Error", e.getMessage());
				e.printStackTrace();
			}
			return mIcon11;
		}

		protected void onPostExecute(Bitmap result) {
			bmImage.setImageBitmap(result);
			category.image = result;
		}
	}

	static class ItemHolder {
		ImageView imgIcon;
		TextView txtTitle;
	}
}
